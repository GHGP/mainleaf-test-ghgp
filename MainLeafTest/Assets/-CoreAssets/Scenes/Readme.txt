Para propósitos de teste a cena "PersistantScene" não é necessária.
Mas os eventos dependentes da interface ou do sistema de carregamento de cenas não serão chamados.

Para testar estes elementos própriamente; carregue a cena "PersistantScene" de forma aditiva e mantenha a variável _loadSceneOnStart no código do SceneDirector desativada caso não queira que a cena inicial seja carregada ao iniciar o modo Play.