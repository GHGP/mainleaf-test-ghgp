﻿using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FieldOfView))]
public class FOV_Gizmo : Editor
{
	private void OnSceneGUI() 
	{
		FieldOfView fov = (FieldOfView)target;

		Handles.color = Color.white;
		Handles.DrawWireArc(fov.transform.position, Vector3.up, Vector3.forward, 360, fov.FovRange);

		Vector3 viewAngle1 = DirectionFromAngle(fov.transform.eulerAngles.y, -fov.FovAngle/2);
		Vector3 viewAngle2 = DirectionFromAngle(fov.transform.eulerAngles.y, fov.FovAngle / 2);

		Handles.color = Color.yellow;
		Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngle1 * fov.FovRange);
		Handles.DrawLine(fov.transform.position, fov.transform.position + viewAngle2 * fov.FovRange);

		Vector3 DirectionFromAngle(float eulerY, float angleDegrees) 
		{
			angleDegrees += eulerY;
			return new Vector3(Mathf.Sin(angleDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleDegrees * Mathf.Deg2Rad));
		}
	}
}
