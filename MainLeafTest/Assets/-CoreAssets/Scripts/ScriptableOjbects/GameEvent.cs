﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameEvent", menuName = "ScriptableObjects/EventSO")]
public class GameEvent : ScriptableObject
{
	private List<EventListener> _listeners = new List<EventListener>();

	public void Invoke()
	{
		for (int i = _listeners.Count - 1; i >= 0; i--)
		{
			_listeners[i].RaiseEvent();
		}
	}

	public void Register(EventListener listener) => _listeners.Add(listener);

	public void Deregister(EventListener listener) => _listeners.Remove(listener);
}