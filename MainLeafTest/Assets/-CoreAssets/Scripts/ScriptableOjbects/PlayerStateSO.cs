﻿using UnityEngine;

public enum PlayerStates
{ FREEMOVE, NOMOVE, CROUCHING, DRAGGING }

[CreateAssetMenu(fileName = "PlayerState", menuName = "ScriptableObjects/PlayerStateTracker")]
public class PlayerStateSO : ScriptableObject
{
	public delegate void StateChangeDelegate();
	public event StateChangeDelegate OnPlayerStateChange;
	[SerializeField] private PlayerStates _currentState;

	public PlayerStates CurrentState
	{
		get => _currentState;
		set { _currentState = value; OnPlayerStateChange?.Invoke(); }
	}
}