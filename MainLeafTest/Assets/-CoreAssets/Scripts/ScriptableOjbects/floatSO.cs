﻿using UnityEngine;

[CreateAssetMenu(fileName = "float", menuName = "ScriptableObjects/float")]
public class floatSO : ScriptableObject
{
	public delegate void ValueChangeDelegate();
	public event ValueChangeDelegate OnValueChange;
	
	[SerializeField] private float _value;
	public float Value
	{
		get => _value;
		set { _value = value; OnValueChange?.Invoke(); }
	}
}