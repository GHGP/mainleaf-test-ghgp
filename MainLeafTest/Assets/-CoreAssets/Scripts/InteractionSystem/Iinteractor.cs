﻿public interface Iinteractor
{
	void OnInteractionTriggerEnter(Iinteractible interactible);

	void OnInteractionTriggerExit();
}