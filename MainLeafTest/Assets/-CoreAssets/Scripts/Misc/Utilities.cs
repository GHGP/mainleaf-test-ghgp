﻿using UnityEngine;

public static class Utilities
{
	public static bool IsGrounded(Transform body)
	{
		const float rayOffset = 0.25f;
		const float maxRayDistance = 0.3f;

		Ray ray = new Ray(body.position + Vector3.up * rayOffset, Vector3.down);

		if (Physics.Raycast(ray, out RaycastHit hit, maxRayDistance)) { return true; }
		else { return false; }
	}
}