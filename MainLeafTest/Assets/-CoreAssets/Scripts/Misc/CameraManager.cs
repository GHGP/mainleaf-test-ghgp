﻿using Cinemachine;
using UnityEngine;

public class CameraManager : MonoBehaviour
{
	[SerializeField] private CinemachineVirtualCamera _startingCamera;
	[SerializeField] private CinemachineVirtualCamera[] _camerasOnScene;

	private void Start()
	{
		if (_startingCamera)
		{
			ResetCamerasPriority();
			_startingCamera.Priority = 10;
		}
	}

	public void ResetCamerasPriority()
	{
		foreach (CinemachineVirtualCamera cam in _camerasOnScene)
		{
			cam.Priority = 1;
		}
	}
}