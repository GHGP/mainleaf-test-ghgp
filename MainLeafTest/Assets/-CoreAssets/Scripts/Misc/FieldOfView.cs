﻿using System.Collections;
using UnityEngine;

public class FieldOfView : MonoBehaviour
{
	[Header("FOV Parameters")]
	public float FovRange;

	[Range(0, 360)] public float FovAngle;
	[SerializeField] private float _fovCheckFrequency = 0.2f;

	[Header("TargetParameters")]
	[SerializeField] private LayerMask _targetLayer;

	[SerializeField] private LayerMask _ObstructionLayer;
	[SerializeField] private GameEvent _onSeeEvent;

	[HideInInspector] public bool IsSeeing;

	private void Start()
	{
		StartCoroutine(FovLoop());
	}

	private IEnumerator FovLoop()
	{
		while (true)
		{
			yield return new WaitForSeconds(_fovCheckFrequency);
			FovCheck();
		}

		void FovCheck()
		{
			Collider[] rangeChecks = Physics.OverlapSphere(transform.position, FovRange, _targetLayer);

			if (rangeChecks.Length != 0)
			{
				Transform target = rangeChecks[0].transform;
				Vector3 offSetTargetPosition = target.position;
				offSetTargetPosition.y += 0.5f;

				Vector3 directionToTarget = (offSetTargetPosition - transform.position).normalized;

				if (Vector3.Angle(transform.forward, directionToTarget) < FovAngle / 2)
				{
					float distanceToTarget = Vector3.Distance(transform.position, target.position);

					if (!Physics.Raycast(transform.position, directionToTarget, out RaycastHit hit, distanceToTarget, _ObstructionLayer))
					{
						IsSeeing = true;
						Debug.DrawRay(transform.position, directionToTarget * distanceToTarget, Color.green);

						_onSeeEvent?.Invoke();
					}
					else
					{
						//Target is behind cover
						Debug.DrawRay(transform.position, directionToTarget * distanceToTarget, Color.red);
						IsSeeing = false;
					}
				}
				else
				{
					//Target out of FOV angle
					IsSeeing = false;
				}
			}
			else
			{
				//There is no target in range
				IsSeeing = false;
			}
		}
	}
}