﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;

public class UIManager : MonoBehaviour
{
	//Singleton
	public static UIManager Instance;

	[Header("References")]
	[SerializeField] private InterfaceInputs _interfaceInputs;
	[SerializeField] private GameObject _endScreen;
	[SerializeField] private GameObject _pauseScreen;
	[SerializeField] private GameObject _gameOverlay;

	[Space]
	[SerializeField] private GameObject _messageScreen;

	[SerializeField] private TMP_Text _messageText;

	//Pause
	private bool _isGamePaused = false;

	//Showmessage
	private Action _onClickAction;

	private void Awake()
	{
		SingletonSetup();
		_interfaceInputs = new InterfaceInputs();

		//hide mouse outside of Editor
		if (!Application.isEditor)	{ Cursor.visible = false; }

		void SingletonSetup()
		{
			if (Instance != null && Instance != this)
			{
				Destroy(gameObject);
			}
			else
			{
				Instance = this;
			}
		}
	}

	private void OnEnable()
	{
		_interfaceInputs.Enable();
		_interfaceInputs.Keyboard.Pause.performed += PauseButtonPressed;
	}

	private void OnDisable()
	{
		_interfaceInputs.Disable();
	}

	private void PauseButtonPressed(InputAction.CallbackContext obj)
	{
		if (!_isGamePaused)
		{
			PauseGame();
		}
		else
		{
			UnPauseGame();
		}
	}

	public void PauseGame()
	{
		if (!Application.isEditor) { Cursor.visible = true; }
		_isGamePaused = true;
		Time.timeScale = 0;
		_pauseScreen.SetActive(true);
		_gameOverlay.SetActive(false);
	}

	public void UnPauseGame() 
	{
		if (!Application.isEditor) { Cursor.visible = false; }
		_isGamePaused = false;
		Time.timeScale = 1;
		_pauseScreen.SetActive(false);
		_gameOverlay.SetActive(true);
	}

	public void ShowMessageOnScreen(string message, Action onContinue)
	{
		_interfaceInputs.Keyboard.Confirm.performed += OnMessageClosed;
		_onClickAction = onContinue;
		_messageScreen.SetActive(true);
		_messageText.text = message;
	}

	private void OnMessageClosed(InputAction.CallbackContext obj)
	{
		_messageScreen.SetActive(false);
		_onClickAction?.Invoke();
		_interfaceInputs.Keyboard.Confirm.performed -= OnMessageClosed;
	}

	public void EndScreen() 
	{
		if (!Application.isEditor) { Cursor.visible = true; }
		_endScreen.SetActive(true);
	}
}