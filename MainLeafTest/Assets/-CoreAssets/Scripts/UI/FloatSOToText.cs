﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using DG.Tweening;

public class FloatSOToText : MonoBehaviour
{
	[SerializeField] private floatSO _floatToTrack;
	[SerializeField] private TMP_Text _targetText;

	private void Start()
	{
		if (_floatToTrack == null) { return; }
		_floatToTrack.OnValueChange += UpdateValues;
		UpdateValues();
	}

	private void OnDestroy()
	{
		_floatToTrack.OnValueChange -= UpdateValues;
	}

	private void UpdateValues() 
	{
		_targetText.text = _floatToTrack.Value.ToString();
	}
}