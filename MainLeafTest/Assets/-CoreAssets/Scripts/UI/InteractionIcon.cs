﻿using UnityEngine;
using DG.Tweening;

public class InteractionIcon : MonoBehaviour
{
	[SerializeField] private floatSO _playerCanInteract;
	[SerializeField] private GameObject _textObject;
	[SerializeField] private float _animationSpeed = 0.5f;

	private void Start()
	{
		_playerCanInteract.OnValueChange += InteractStateChanged;
	}

	private void OnDestroy()
	{
		_playerCanInteract.OnValueChange -= InteractStateChanged;
	}

	private void InteractStateChanged() 
	{
		_textObject.SetActive(false);
		
		if (_playerCanInteract.Value == 1)
		{
			_textObject.SetActive(true);
		}
	}
}