﻿using UnityEngine;
using UnityEngine.AI;

public class GuardAnimations : MonoBehaviour
{
	[SerializeField] private Animator _guardAnimator;
	private NavMeshAgent _agent;

	private void Start()
	{
		_agent = GetComponent<NavMeshAgent>();
	}

	private void Update()
	{
		if (_guardAnimator == null) { return; }

		_guardAnimator.SetFloat("Movement", _agent.velocity.magnitude);
	}
}