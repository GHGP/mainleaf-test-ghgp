﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public enum GuardState
{ IDLE, PATROLLING }

public class PatrollingGuard : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private Transform[] _waypoints;
	public NavMeshAgent Agent;

	[Header("Patrol Values")]
	[SerializeField] private float _standTimer;
	[SerializeField] private float _movementSpeed = 3.5f;
	[SerializeField] private int _startingWaipoint;
	[SerializeField] private bool _startOnSceneLoad = false;

	//Patrol trackers
	private GuardState _currentState;
	private int _currentWaypointIndex = 0;

	private void Start()
	{
		Agent = GetComponent<NavMeshAgent>();
		Agent.isStopped = true;
		Agent.speed = _movementSpeed;
		_currentWaypointIndex = _startingWaipoint;
		Agent.SetDestination(_waypoints[_currentWaypointIndex].position);

		if (_startOnSceneLoad) { StartPatrol(); }
	}

	private void FixedUpdate()
	{
		if (_currentState != GuardState.PATROLLING) { return; }

		Vector3 currentPositionXZ = transform.position;
		Vector3 targetPositionXZ = _waypoints[_currentWaypointIndex].position;
		currentPositionXZ.y = 0;
		targetPositionXZ.y = 0;

		if (Vector3.Distance(currentPositionXZ, targetPositionXZ) <= 0.1f)
		{
			transform.position = _waypoints[_currentWaypointIndex].position;
			StartCoroutine(StandGuard());
		}
	}

	private void OnCollisionEnter(Collision collision)
	{
		//In case the player collides with the Guard
		if (collision.gameObject.layer == 10)
		{
			Vector3 normalisedPlayerPosition = collision.transform.position;
			normalisedPlayerPosition.y = 0;

			transform.LookAt(normalisedPlayerPosition);
		}
	}

	public void StartPatrol()
	{
		_currentState = GuardState.PATROLLING;
		Agent.isStopped = false;
	}

	public void StopPatrol()
	{
		_currentState = GuardState.IDLE;
		Agent.isStopped = true;
	}

	private void GetNextDestination()
	{
		GetNextWaypoint();

		Vector3 targetPoint = _waypoints[_currentWaypointIndex].position;
		Agent.SetDestination(targetPoint);

		void GetNextWaypoint()
		{
			_currentWaypointIndex++;
			if (_currentWaypointIndex == _waypoints.Length)
			{
				_currentWaypointIndex = 0;
			}
		}
	}

	private IEnumerator StandGuard()
	{
		GetNextDestination();
		Agent.isStopped = true;

		yield return new WaitForSeconds(_standTimer);

		Agent.isStopped = false;
	}
}