﻿using DG.Tweening;
using UnityEngine;

public class Rupee : MonoBehaviour
{
	[SerializeField] private float _rupeeAmmount;
	[SerializeField] private floatSO _playerWallet;
	private BoxCollider _collider;

	private void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.layer == 10)
		{
			_playerWallet.Value += _rupeeAmmount;
			_collider.enabled = false;

			//This would normaly be part of an Object pooling system if the project was larger
			//PickupAnimation
			transform.DOPunchPosition(new Vector3(0, 1f, 0), 0.5f, 0, 0).
				OnComplete(() => Destroy(gameObject));
		}
	}

	private void Start()
	{
		_collider = GetComponent<BoxCollider>();

		//Rotation animation
		Vector3 rot = new Vector3(0, 180, 0);
		transform.DOLocalRotate(rot, 4f, RotateMode.Fast).SetLoops(-1).SetEase(Ease.Linear);
	}
}