﻿using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.InputSystem;

public class DraggableBlock : MonoBehaviour, Iinteractible
{
	[Header("References")]
	[SerializeField] private GameObject _interactionTriggers;
	[SerializeField] private PhysicMaterial _airborneMaterial;

	private PlayerInputs _playerInputs;
	private GameObject _playerObject;
	private Rigidbody _rb;
	private BoxCollider _collider;

	[Header("Drag Parameters")]
	[SerializeField] private LayerMask _avoidColisionWith;
	[SerializeField] private float _pushDistance = 1;
	[SerializeField] private float _pushSpeed = 4;

	//State Trackers
	private bool _isPushing = false;
	private bool _isPulling = false;
	private bool _isOnDragMode = false;

	//Dragging logic
	private Vector3 _boxdestination = Vector3.zero;
	private Vector3 _moveDirection;
	private List<Task> _activeTasks = new List<Task>();

	private void Awake()
	{
		_collider = GetComponent<BoxCollider>();
		_rb = GetComponent<Rigidbody>();
		_playerInputs = new PlayerInputs();
		_playerInputs.Enable();
	}

	private void OnDestroy()
	{
		_playerInputs.Disable();
	}

	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.layer == 10)
		{
			PlayerController.Instance.PlayerRB.AddForce(Vector3.up, ForceMode.Impulse);
		}
	}

	private void FixedUpdate()
	{
		OnAirborne();

		if (!_isOnDragMode) { return; }
		GrabInputs();
		MoveBox();

		void GrabInputs()
		{
			if (_playerInputs.Player.Push.ReadValue<float>() > 0)
			{
				if (!_isPulling && !_isPushing)
				{
					_moveDirection = _playerObject.transform.forward;
					if (!CheckForspace()) { return; }

					_activeTasks.Add(PushWithCoolDown());
				}
			}

			if (_playerInputs.Player.Push.ReadValue<float>() < 0)
			{
				if (!_isPulling && !_isPushing)
				{
					_moveDirection = (_playerObject.transform.forward * -1);
					if (!CheckForspace()) { return; }

					_activeTasks.Add(PullWithCoolDown());
				}
			}
		}

		void MoveBox()
		{
			if (!_isPushing && !_isPulling) { return; }

			Rigidbody playerRB = PlayerController.Instance.PlayerRB;
			Vector3 playerPosition = _playerObject.transform.position;

			if (Vector3.Distance(transform.position, _boxdestination) <= 0.1f)
			{
				transform.position = _boxdestination;
				_boxdestination = Vector3.zero;

				return;
			}

			if (_boxdestination != Vector3.zero)
			{
				_rb.MovePosition(transform.position + _moveDirection * _pushSpeed * Time.deltaTime);
				playerRB.MovePosition(playerPosition + (_moveDirection * _pushSpeed * Time.deltaTime));
			}
		}

		void OnAirborne()
		{
			if (_rb.isKinematic) { return; }

			if (Utilities.IsGrounded(transform))
			{
				_collider.material = null;
				_rb.isKinematic = true;
			}
			else
			{
				_collider.material = _airborneMaterial;
			}
		}
	}

	//-- Interaction Methods --

	public void OnInteract()
	{
		PlayerController.Instance.StartDragMode(gameObject, DragInitiated);
	}

	private async void InteractButtonReleased(InputAction.CallbackContext obj)
	{
		Debug.Log("RELEASE");
		await Task.WhenAll(_activeTasks);
		EndDrag();
	}

	public void DisableInteracions()
	{
		_rb.constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
		_interactionTriggers.SetActive(false);
		PlayerController.Instance.OnInteractionTriggerExit();
	}

	public void DragInitiated()
	{
		Debug.Log("PRESS");
		_playerObject = PlayerController.Instance.gameObject;
		_isOnDragMode = true;
		_playerInputs.Player.Interact.canceled += InteractButtonReleased;
	}

	public void EndDrag()
	{
		PlayerController.Instance.ResetMode();
		_playerObject = null;
		_isOnDragMode = false;
		_playerInputs.Player.Interact.canceled -= InteractButtonReleased;
	}

	//Drag logic Methods

	private bool CheckForspace()
	{
		Vector3 offsetPosition = transform.position;
		offsetPosition.y -= 0.6f;

		Ray ray = new Ray(offsetPosition, _moveDirection.normalized);
		if (Physics.Raycast(ray, out RaycastHit hit, 1.5f, _avoidColisionWith))
		{
			Debug.Log(hit.transform.gameObject.name);
			Debug.DrawRay(offsetPosition, _moveDirection * 1.5f, Color.red);
			return false;
		}
		else
		{
			//Debug.Log("NOHIT");
			Debug.DrawRay(offsetPosition, _moveDirection * 1.5f, Color.red);
			return true;
		}
	}

	private async Task PushWithCoolDown()
	{
		_isPushing = true;
		_boxdestination = transform.position + _moveDirection * _pushDistance;

		await Task.Delay(1000);
		_isPushing = false;
		_boxdestination = Vector3.zero;
	}

	private async Task PullWithCoolDown()
	{
		_isPulling = true;
		_boxdestination = transform.position + _moveDirection * _pushDistance;

		await Task.Delay(1000);
		_isPulling = false;
		_boxdestination = Vector3.zero;
	}
}