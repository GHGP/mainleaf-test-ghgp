﻿using UnityEngine;

public class PlayerAnimations : MonoBehaviour
{
	[Header("References")]
	private PlayerInputs _playerInputs;
	[SerializeField] private PlayerStateSO _playerState;
	[SerializeField] private Animator _animator;
	[SerializeField] private Rigidbody _rigidBody;
	private readonly float _maxSpeed = 0.6f;

	private void Awake()
	{
		_playerState.OnPlayerStateChange += OnStateChange;
		_playerInputs = new PlayerInputs();
	}

	private void OnEnable()
	{
		_playerInputs.Enable();
	}

	private void OnDisable()
	{
		_playerInputs.Disable();
		_playerState.OnPlayerStateChange -= OnStateChange;
	}

	private void Update()
	{
		if (_playerState.CurrentState == PlayerStates.FREEMOVE || _playerState.CurrentState == PlayerStates.CROUCHING) { FreemoveAnimations(); }

		if (_playerState.CurrentState == PlayerStates.DRAGGING) { DragAnimations(); }

		void FreemoveAnimations()
		{
			if (Utilities.IsGrounded(transform))
			{
				//Track if is jumping
				_animator.SetBool("IsAirborne", false);
				_animator.SetBool("IsGrounded", true);

				//Track if is moving
				_animator.SetFloat("moveSpeed", _rigidBody.velocity.magnitude / _maxSpeed);
			}
			else
			{
				_animator.SetBool("IsGrounded", false);
				_animator.SetBool("IsAirborne", true);
			}
		}

		void DragAnimations()
		{
			if (_playerInputs.Player.Push.ReadValue<float>() > 0)
			{
				_animator.SetBool("IsPushing", true);
				_animator.SetBool("IsPulling", false);
			}
			else if (_playerInputs.Player.Push.ReadValue<float>() < 0)
			{
				_animator.SetBool("IsPushing", false);
				_animator.SetBool("IsPulling", true);
			}
			else
			{
				_animator.SetBool("IsPushing", false);
				_animator.SetBool("IsPulling", false);
			}
		}
	}

	private void OnStateChange()
	{
		switch (_playerState.CurrentState)
		{
			case PlayerStates.FREEMOVE:
				ResetAnimations();
				break;

			case PlayerStates.DRAGGING:
				_animator.SetBool("IsDragging", true);
				break;

			case PlayerStates.CROUCHING:
				_animator.SetBool("IsCrouching", true);
				break;

			case PlayerStates.NOMOVE:
				_animator.SetFloat("moveSpeed", 0);
				PlayerController.Instance.PlayerRB.velocity = Vector3.zero;
				break;

			default:
				break;
		}

		void ResetAnimations()
		{
			_animator.SetBool("IsDragging", false);
			_animator.SetBool("IsCrouching", false);
			_animator.SetBool("IsPushing", false);
			_animator.SetBool("IsPulling", false);
		}
	}
}