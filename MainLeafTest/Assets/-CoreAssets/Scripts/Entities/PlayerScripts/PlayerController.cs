﻿using Cinemachine;
using System;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour, Iinteractor
{
	// ----- Variables -----

	public static PlayerController Instance { get; private set; }

	[Header("References")]
	private PlayerInputs _playerInputs;
	private CapsuleCollider _collider;
	public Rigidbody PlayerRB;
	public PhysicMaterial airborneMaterial;
	
	[SerializeField] private CinemachineVirtualCamera _focusCamera;
	private Transform _cameraTransform;

	public PlayerStateSO _playerState;
	public floatSO _playerCanInteractSO;

	[Header("Movement Values")]
	[SerializeField] private float _jumpStrength = 5f;
	[SerializeField] private float _movementSpeed = 5f;
	[SerializeField] private float _maxSpeed;
	private Vector3 _forceDirection = Vector3.zero;

	[Header("Collider Interactions")]
	private Iinteractible _interactibleObject;

	private void Awake()
	{
		SingletonSetup();

		PlayerRB = GetComponent<Rigidbody>();
		_collider = GetComponent<CapsuleCollider>();
		_playerInputs = new PlayerInputs();
		_playerState.CurrentState = PlayerStates.FREEMOVE;
		_cameraTransform = Camera.main.transform;

		void SingletonSetup()
		{
			if (Instance != null && Instance != this)
			{
				Destroy(gameObject);
			}
			else
			{
				Instance = this;
			}
		}
	}

	private void OnEnable()
	{
		_playerInputs.Player.Jump.performed += Jump;
		_playerInputs.Player.Crouch.performed += StartCrouch;
		_playerInputs.Player.Crouch.canceled += CrouchButtonReleased;
		_playerInputs.Player.Interact.performed += OnInteractPress;
		_playerInputs.Player.Enable();
	}

	private void OnDisable()
	{
		_playerInputs.Player.Jump.performed -= Jump;
		_playerInputs.Player.Crouch.performed -= StartCrouch;
		_playerInputs.Player.Crouch.canceled -= CrouchButtonReleased;
		_playerInputs.Player.Interact.performed -= OnInteractPress;
		_playerInputs.Player.Disable();
	}

	private void FixedUpdate()
	{
		if (_playerState.CurrentState == PlayerStates.FREEMOVE || _playerState.CurrentState == PlayerStates.CROUCHING)
		{
			MovePlayer();
			PlayerLookAt();
		}

		PlayerDropAcceleration();
		LimitPlayerSpeed();
		NoFrictionOnAir();

		// Local Methods
		void MovePlayer()
		{
			_forceDirection += _playerInputs.Player.Move.ReadValue<Vector2>().x * GetcameraRight(_cameraTransform) * _movementSpeed;
			_forceDirection += _playerInputs.Player.Move.ReadValue<Vector2>().y * GetcameraFoward(_cameraTransform) * _movementSpeed;

			PlayerRB.AddForce(_forceDirection, ForceMode.Impulse);
			_forceDirection = Vector3.zero;

			Vector3 GetcameraRight(Transform cameraTransform)
			{
				Vector3 right = cameraTransform.transform.right;
				right.y = 0;
				return right.normalized;
			}

			Vector3 GetcameraFoward(Transform cameraTransform)
			{
				Vector3 foward = cameraTransform.transform.forward;
				foward.y = 0;
				return foward.normalized;
			}
		}

		void PlayerLookAt()
		{
			Vector3 direction = PlayerRB.velocity;
			direction.y = 0f;

			if (_playerInputs.Player.Move.ReadValue<Vector2>().sqrMagnitude > 0.1f && direction.sqrMagnitude > 0.1f)
			{
				PlayerRB.rotation = Quaternion.LookRotation(direction, Vector3.up);
			}
			else
			{
				PlayerRB.angularVelocity = Vector3.zero;
			}
		}

		void PlayerDropAcceleration()
		{
			if (PlayerRB.velocity.y < 0f)
			{
				PlayerRB.velocity -= Vector3.down * Physics.gravity.y * 2f * Time.fixedDeltaTime;
			}
		}

		void LimitPlayerSpeed()
		{
			Vector3 horizontalVelocity = PlayerRB.velocity;
			horizontalVelocity.y = 0;

			if (horizontalVelocity.sqrMagnitude > _maxSpeed * _maxSpeed)
			{
				PlayerRB.velocity = horizontalVelocity.normalized * _maxSpeed + Vector3.up * PlayerRB.velocity.y;
			}
		}

		void NoFrictionOnAir()
		{
			if (Utilities.IsGrounded(transform))
			{
				_collider.material = null;
			}
			else
			{
				_collider.material = airborneMaterial;
			}
		}
	}

	private void Jump(InputAction.CallbackContext context)
	{
		if (!Utilities.IsGrounded(transform)) { return; }
		if (_playerState.CurrentState != PlayerStates.FREEMOVE && _playerState.CurrentState != PlayerStates.CROUCHING) { return; }

		_forceDirection += Vector3.up * _jumpStrength;
	}

	private void StartCrouch(InputAction.CallbackContext context)
	{
		if (_playerState.CurrentState != PlayerStates.FREEMOVE) { return; }
		_playerState.CurrentState = PlayerStates.CROUCHING;

		_collider.center = new Vector3(0, 0.85f, 0);
		_collider.height = 1.3f;
	}

	private void CrouchButtonReleased(InputAction.CallbackContext context)
	{
		EndCrouch();
	}

	private void EndCrouch()
	{
		_playerState.CurrentState = PlayerStates.FREEMOVE;
		_collider.center = new Vector3(0, 0.9f, 0);
		_collider.height = 1.6f;
	}

	

	public void ResetMode()
	{
		PlayerRB.freezeRotation = false;
		PlayerRB.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
		_playerState.CurrentState = PlayerStates.FREEMOVE;
		_focusCamera.Priority = 1;
	}

	// -- Interacion System --

	private void OnInteractPress(InputAction.CallbackContext obj)
	{
		if (!Utilities.IsGrounded(transform)) { return; }

		if (_interactibleObject != null)
		{
			_interactibleObject.OnInteract();
			_focusCamera.Priority = 99;
		}
	}

	public void OnInteractionTriggerEnter(Iinteractible interactible)
	{
		_interactibleObject = interactible;
		_playerCanInteractSO.Value = 1;
	}

	public void OnInteractionTriggerExit()
	{
		_interactibleObject = null;
		_playerCanInteractSO.Value = 0;
	}

	public async void StartDragMode(GameObject dragObject, Action setupEndedCallback)
	{
		_playerState.CurrentState = PlayerStates.DRAGGING;

		ResetPlayerPosition();

		await Task.Delay(100);

		setupEndedCallback?.Invoke();

		void ResetPlayerPosition()
		{
			//Aligb player rotation with the normal of the cube he will grab
			PlayerRB.velocity = Vector3.zero;
			Vector3 cubedirection = (dragObject.transform.position - transform.position);
			Vector3 offsetPosition = transform.position;
			offsetPosition.y += 1;

			Ray ray = new Ray(offsetPosition, cubedirection);
			if (Physics.Raycast(ray, out RaycastHit hit, 100))
			{
				PlayerRB.rotation = Quaternion.LookRotation(hit.normal * -1);
				PlayerRB.freezeRotation = true;
			}
		}
	}
}