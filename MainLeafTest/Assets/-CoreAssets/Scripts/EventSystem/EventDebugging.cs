﻿using UnityEngine;

public class EventDebugging : MonoBehaviour
{
	public void DebugMessage(string message)
	{
		Debug.Log(message);
	}
}