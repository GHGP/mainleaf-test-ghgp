﻿using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;

public class DelayedEventListener : EventListener
{
	[SerializeField] private float _delayTime = 1;
	[SerializeField] private UnityEvent _delayedUnityEvent;

	public override void RaiseEvent()
	{
		_unityEvent.Invoke();
		RunEventDelayed();
	}

	private async void RunEventDelayed()
	{
		await Task.Delay((int)_delayTime * 1000);

		_delayedUnityEvent.Invoke();
	}
}