﻿using UnityEngine;

public class DemoEnd : MonoBehaviour
{
	public void EndTest()
	{
		UIManager.Instance?.EndScreen();
		SceneDirector.Instance?.LoadScene(SceneIndex.END);
	}
}