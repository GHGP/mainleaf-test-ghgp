﻿using UnityEngine;

public class SpawnerArea01 : MonoBehaviour
{
	[Header("References")]
	[SerializeField] private GameObject _playerObject;
	[SerializeField] private GameObject[] _draggableBlocks;
	[SerializeField] private GameObject _puzzleTrigger;

	[Header("Data")]
	[SerializeField] private floatSO _isPlayerCaught;
	[SerializeField] private floatSO _isPuzzleCompleted;

	[Header("SpawnPoints")]
	[SerializeField] private Transform _playerSpawnTransition;
	[SerializeField] private Transform _playerSpawnCaught;
	[Space]
	[SerializeField] private Transform[] _blocksSpawn;

	private void Start()
	{
		SetupPlayer();
		SetupBlock();

		void SetupPlayer()
		{
			if (_isPlayerCaught.Value == 1)
			{
				_playerObject.transform.position = _playerSpawnCaught.position;
				_playerObject.transform.rotation = _playerSpawnCaught.rotation;
				_isPlayerCaught.Value = 0;
			}
			else if (_isPuzzleCompleted.Value == 1)
			{
				//If the puzzle is complete and the player isnt caught then he just transitioned back
				_playerObject.transform.position = _playerSpawnTransition.position;
				_playerObject.transform.rotation = _playerSpawnTransition.rotation;
			}
		}

		void SetupBlock()
		{
			if (_isPuzzleCompleted.Value == 1)
			{
				for (int i = 0; i < _draggableBlocks.Length; i++)
				{
					_draggableBlocks[i].transform.position = _blocksSpawn[i].position;
					var blockCode = _draggableBlocks[i].GetComponent<DraggableBlock>();
					blockCode.EndDrag();
					blockCode.DisableInteracions();
				}

				_puzzleTrigger.SetActive(false);
			}
		}
	}
}