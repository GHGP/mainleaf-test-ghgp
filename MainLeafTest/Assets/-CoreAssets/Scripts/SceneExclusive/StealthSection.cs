﻿using UnityEngine;

public class StealthSection : MonoBehaviour
{
	[SerializeField] private PlayerStateSO _playerState;
	[SerializeField] private floatSO _playerCaughtSO;
	[SerializeField] private PatrollingGuard[] _guards;

	[Space]
	[SerializeField] private SceneIndex _targetScene;

	[SerializeField, TextArea] private string _messageToShow;

	public void OnPlayerDiscovered()
	{
		for (int i = 0; i < _guards.Length; i++)
		{
			_guards[i].StopPatrol();
		}
		_playerState.CurrentState = PlayerStates.NOMOVE;
		UIManager.Instance?.ShowMessageOnScreen(_messageToShow, SendPlayerToScene);
	}

	private void SendPlayerToScene()
	{
		_playerCaughtSO.Value = 1f;
		_playerState.CurrentState = PlayerStates.FREEMOVE;
		SceneDirector.Instance?.LoadScene(_targetScene);
	}
}