﻿using UnityEngine;

public class InteractionTrigger : CustomTrigger
{
	public GameObject parentGO;
	private Iinteractible parent;
	private Iinteractor currentInteractor;

	private void Awake() => parent = parentGO.transform.GetComponent<Iinteractible>();
	protected override void OnTriggerActivated(Collider other = null)
	{
		base.OnTriggerActivated(other);

		if (other.TryGetComponent(out Iinteractor interactor))
		{
			currentInteractor = interactor;
			interactor.OnInteractionTriggerEnter(parent);
		}
	}

	private void OnTriggerExit(Collider other)
	{
		if (currentInteractor != null)
		{
			currentInteractor.OnInteractionTriggerExit();
			currentInteractor = null;
		}
	}
}