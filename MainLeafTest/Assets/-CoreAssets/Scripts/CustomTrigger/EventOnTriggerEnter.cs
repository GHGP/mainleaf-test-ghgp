﻿using UnityEngine;
using UnityEngine.Events;

public class EventOnTriggerEnter : CustomTrigger
{
	[SerializeField] private GameEvent _event;
	[SerializeField] private UnityEvent _onTriggerAction;

	protected override void OnTriggerActivated(Collider other)
	{
		base.OnTriggerActivated();

		_event?.Invoke();
		_onTriggerAction?.Invoke();
	}
}