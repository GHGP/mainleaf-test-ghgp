﻿using UnityEngine;

public class DraggableBlockTrigger : CustomTrigger
{
	[SerializeField] private GameEvent blockInPositionEvent;
	private GameObject _currentBlock;

	protected override void OnTriggerActivated(Collider other)
	{
		base.OnTriggerActivated();
		_currentBlock = other.gameObject;
	}

	private void OnTriggerExit(Collider other)
	{
		if (_currentBlock != null)
		{
			_currentBlock = null;
		}
	}

	private void Update()
	{
		if (_currentBlock == null) { return; }

		float distanceToCenter = Vector3.Distance(transform.position, _currentBlock.transform.position);

		if (distanceToCenter <= 0.1)
		{
			_currentBlock.transform.position = transform.position;
			_currentBlock.GetComponent<EventListener>().RaiseEvent();
		}
	}
}