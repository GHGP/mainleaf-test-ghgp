﻿using UnityEngine;

public class SceneChangeTrigger : CustomTrigger
{
	[SerializeField] private SceneIndex _sceneToLoad;

	protected override void OnTriggerActivated(Collider other = null)
	{
		base.OnTriggerActivated(other);
		SceneDirector.Instance?.LoadScene(_sceneToLoad);
	}
}