﻿using UnityEngine;

public class CustomTrigger : MonoBehaviour
{
	[SerializeField] protected int _triggerableLayer = 10;
	[SerializeField] protected bool _onlyTriggerOnce;
	protected bool _alreadyTriggered = false;

	private void OnTriggerEnter(Collider other)
	{
		if (_onlyTriggerOnce && _alreadyTriggered) { return; }

		if (other.gameObject.layer == _triggerableLayer)
		{
			OnTriggerActivated(other);
		}
	}

	protected virtual void OnTriggerActivated(Collider other = null) => _alreadyTriggered = true;
}