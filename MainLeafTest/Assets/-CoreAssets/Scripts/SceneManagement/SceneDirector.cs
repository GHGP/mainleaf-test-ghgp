﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public enum SceneIndex
{ AREA1, AREA2, END }

public class SceneDirector : MonoBehaviour
{
	public static SceneDirector Instance;

	[Header("References")]
	[Tooltip("Stop's the initial scene from Auto-loading if checked")]
	[SerializeField] private bool _loadSceneOnStart;

	[SerializeField] private GameObject _loadScreen;
	[SerializeField] private SceneIndex _initialScene;
	[SerializeField] private AssetReference[] _sceneReferences;

	//----- AddressableLoading ---
	private List<AsyncOperation> currentOperations = new List<AsyncOperation>();
	private bool _clearPreviousScene = false;
	private SceneInstance _previousLoadedScene;

	private void Start()
	{
		SingletonSetup();

		if (!_loadSceneOnStart) { return; }
		LoadScene(_initialScene);

		void SingletonSetup()
		{
			if (Instance != null && Instance != this)
			{
				Destroy(gameObject);
			}
			else
			{
				Instance = this;
			}
		}
	}

	public void RestartGame()
	{
		UIManager.Instance.UnPauseGame();
		LoadScene(SceneIndex.AREA1);
	}

	public void QuitGame()
	{
		Application.Quit();
	}

	public void LoadScene(SceneIndex scene)
	{
		_loadScreen.SetActive(true);
		StartCoroutine(LoadAddressableScene((int)scene));
	}

	private IEnumerator LoadAddressableScene(int sceneIndex)
	{
		//If we are comming form another scene. Unload it
		if (_clearPreviousScene)
		{
			AsyncOperationHandle<SceneInstance> handle = Addressables.UnloadSceneAsync(_previousLoadedScene);

			yield return handle;

			_clearPreviousScene = false;
			_previousLoadedScene = new SceneInstance();
		}

		//Load the new Scene
		Addressables.LoadSceneAsync(_sceneReferences[sceneIndex], LoadSceneMode.Additive, true).Completed += OnNewSceneLoaded; ;

		void OnNewSceneLoaded(AsyncOperationHandle<SceneInstance> obj)
		{
			if (obj.Status == AsyncOperationStatus.Succeeded)
			{
				_clearPreviousScene = true;
				_previousLoadedScene = obj.Result;

				Scene s = obj.Result.Scene;
				SceneManager.SetActiveScene(s);

				StartCoroutine(GetSceneLoadProgress());
			}
		}
	}

	public IEnumerator GetSceneLoadProgress()
	{
		for (int i = 0; i < currentOperations.Count; i++)
		{
			while (!currentOperations[i].isDone)
			{
				yield return null;
			}
		}

		_loadScreen?.SetActive(false);
	}
}