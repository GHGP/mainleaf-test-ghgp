﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.SceneManagement;

public class GameInitialiser : MonoBehaviour
{
	[SerializeField] private AssetReference _initialScene;

	private void Awake()
	{
		//Load the new Scene
		Addressables.LoadSceneAsync(_initialScene, LoadSceneMode.Single ,true);
	}
}
