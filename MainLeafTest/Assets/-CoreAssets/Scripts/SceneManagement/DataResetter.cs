﻿using UnityEngine;

public class DataResetter : MonoBehaviour
{
	public floatSO[] _floatsToResetOnPlay;

	private void Start()
	{
		ResetData();
	}

	public void ResetData()
	{
		foreach (floatSO floatSO in _floatsToResetOnPlay)
		{
			floatSO.Value = 0;
		}
	}
}